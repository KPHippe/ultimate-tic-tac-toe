import pytest 

import Main

def test_gameWinnerHorizontal():


    noneBoard = [[0,0,0],
                 [0,0,0],
                 [0,0,0]]
    p1WinBoard = [[0,1,0],
                  [1,1,1],
                  [0,2,2]]
    p2WinBoard = [[2,2,2],
                  [1,2,0],
                  [0,1,1]]
    

    assert Main.getWinner(noneBoard) == 0 
    assert Main.getWinner(p1WinBoard) == 1 
    assert Main.getWinner(p2WinBoard) == 2
def test_gameWinnerVertical():

    noneBoard = [[0,0,0],
                 [0,0,0],
                 [0,0,0]]
    p1WinBoard = [[0,1,0],
                  [2,1,2],
                  [0,1,2]]
    p2WinBoard = [[1,2,1],
                  [1,2,0],
                  [0,2,1]]


    assert Main.getWinner(noneBoard) == 0 
    assert Main.getWinner(p1WinBoard) == 1
    assert Main.getWinner(p2WinBoard) == 2

def test_gameWinnerDiagonal():
    noneBoard = [[0,0,0],
                 [0,0,0],
                 [0,0,0]]
    p1WinBoard = [[1,1,0],
                  [2,1,2],
                  [0,2,1]]
    p2WinBoard = [[2,2,1],
                  [1,2,0],
                  [1,1,2]]


    assert Main.getWinner(noneBoard) == 0 
    assert Main.getWinner(p1WinBoard) == 1 
    assert Main.getWinner(p2WinBoard) == 2

