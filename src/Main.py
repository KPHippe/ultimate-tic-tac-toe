import random
import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.


# game loop
if __name__ == "__main__":
    while True:
        opponent_row, opponent_col = [int(i) for i in input().split()]
        valid_action_count = int(input())
        
        validActions = []
        for i in range(valid_action_count):
            row, col = [int(j) for j in input().split()]
        

            validActions.append(f"{row} {col}")
        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr)
        
        move = [str(random.randint(0, 2)), str(random.randint(0, 2))]
        strMove = " ".join(move)
        while strMove not in validActions:
            move = [str(random.randint(0, 2)), str(random.randint(0, 2))]
            strMove = " ".join(move)




        print(strMove)


def getWinner(board):

    for i in range(len(board)):
        if board[0][i] != 0 and board[0][i] == board[1][i] and board[1][i] == board[2][i]:
            return board[0][i]
        if board[i][0] != 0 and board[i][0] == board[i][1] and board[i][1] == board[i][2]:
            return board[i][0]
    

    if board[0][0] != 0 and board[0][0] == board[1][1] and board[1][1] == board[2][2]:
        return board[0][0]
    if board[0][2] != 0 and board[0][2] == board[1][1] and board[1][1] == boar[2][0]:
        return board[0][2]
    return 0 
